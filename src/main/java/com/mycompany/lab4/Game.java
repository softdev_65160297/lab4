/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author paewnosuke
 */
public class Game {
    private Player o;
    private Player x;
    private int row;
    private int col;
    private Table Table;
    Scanner sc = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }
    public void newBoard() {
        this.Table = new Table(o, x);
    }
    
    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    public void showTurn() {
        Player player = Table.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }
    public void showTable() {
        char[][] table = this.Table.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }
    public void inputRowCol() {
        while(true) {
            System.out.print("Please input row, col:");
            row = sc.nextInt();
            col = sc.nextInt();
            if(Table.setRowCol(row, col)) {
                return;
            }
        }
    }
    
    public boolean isFinish() {
        if(Table.isDraw()|| Table.isWin()) {
            return true;
        }
        return false;
    }
    public void showStat() {
        System.out.println(o);
        System.out.println(x);
    }
    public void showResult() {
        if(Table.isDraw()) {
            System.out.println("Draw!!!");
        } else if(Table.isWin()) {
            System.out.println(Table.getCurrentPlayer().getSymbol() + " Win");
        }
    }
    
}